//
//  ViewController.swift
//  Flower CoreML
//
//  Created by iulian david on 8/10/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit
import MobileCoreServices
import CoreML
import Vision
import Alamofire
import SwiftyJSON
import SDWebImage


let BASE_WIKIPEDIA_URL = "https://en.wikipedia.org/w/api.php"
class ViewController: UIViewController {
    
    @IBOutlet weak var selectPhoto: UIBarButtonItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var extractLabel: UILabel!
    
    @IBAction func handleUpload(_ sender: UIBarButtonItem) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        } else {
            imagePicker.mediaTypes = [kUTTypeImage as String]
        }
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion:nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        
        // Take the image
        if let editedImage = info[UIImagePickerControllerEditedImage] as! UIImage? {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as! UIImage? {
            selectedImageFromPicker = originalImage
        }
        if let selectedImage = selectedImageFromPicker {
//            imageView.image = selectedImage
            guard let ciImage = CIImage(image: selectedImage) else {
                fatalError("Could not convert the image to a CIImage")
            }
            detect(image: ciImage)
        }
        
        
        
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func detect(image: CIImage) {
        
        guard let model = try? VNCoreMLModel(for: FlowerClassifier().model) else {
            fatalError("Loading CoreML Model failed")
        }
        
        let request = VNCoreMLRequest(model: model) { (request, error) in
            guard let classification = request.results?.first as? VNClassificationObservation else {
                fatalError("model failed to process image")
            }
            let classificationName: String = classification.identifier
            self.navigationItem.title = classificationName.capitalized
            let params: [String: Any] =
                [
                    "format":"json",
                    "action": "query",
                    "prop" : "extracts|pageimages",
                    "exintro":"",
                    "explaintext": "",
                    "titles": classificationName,
                    "indexpageids": "",
                    "redirects": 1,
                    "pithumbsize": 500
                ]
            
            Alamofire.request("\(BASE_WIKIPEDIA_URL)", method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
                .validate(statusCode: 200..<300)
                .responseJSON { [weak self] (response) in
                    switch response.result {
                    case .success(_) :
                        let data = JSON(response.result.value)
                        if let pageid = data["query"]["pageids"][0].string, let extract = data["query"]["pages"][pageid]["extract"].string, let flowerImageURL = data["query"]["pages"][pageid]["thumbnail"]["source"].string
                        {
                            guard let strongSelf = self else {
                                return
                            }
                            
                            strongSelf.imageView.sd_setImage(with: URL(string: flowerImageURL))
                            strongSelf.extractLabel.text = extract
                            strongSelf.extractLabel.sizeToFit()
                            print(extract)
                        }
                        
                        
                        
                            
                        
                    case .failure(_) :
                        debugPrint("Alamofire request failed: \(String(describing: response.result.error))")
                    }
            }
        }
        
        let handler = VNImageRequestHandler(ciImage: image)
        do {
            try handler.perform([request])
        } catch let error {
            print(error)
        }
        
    }
    
}
