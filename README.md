# README #

An example of using Caffe Model with CoreML

The project contains the already converted Caffe model [DataSet](http://www.robots.ox.ac.uk/%7Evgg/data/flowers/102/102flowers.tgz)

### Converting the model

### Install coremltools
### Run **convert-script.py** from the root project in the directory where the extracted archive downloaded